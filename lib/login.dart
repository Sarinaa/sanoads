import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatelessWidget {
  LoginPage({super.key});
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  Future<void> loginUser(context) async {
    String url = 'http://localhost:4000/login';

    Map<String, String> loginData = {
      'email': emailController.text,
      'password': passwordController.text,
    };

    try {
      http.Response response = await http.post(Uri.parse(url),
          body: jsonEncode(loginData),
          headers: {"Content-Type": "application/json"});

      if (response.statusCode == 200) {
        // ignore: avoid_print
        print("Login Successfully");
      } else {
        // ignore: avoid_print
        print('Login failed');
      }
    } on Exception catch (e) {
      // Catch and handle connection-related exceptions
      // ignore: avoid_print
      print('Exception occurred: $e');
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Error'),
          content: const Text(
              'Unable to connect to the server. Please try again later.'),
          actions: [
            ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: SafeArea(
              child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(18, 50, 0, 0),
                child: Text(
                  'Sanoads',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
                child: SizedBox(
                  height: 60,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                    child: TextField(
                      controller: emailController,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Username/Email'),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: SizedBox(
                  height: 60,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                    child: TextField(
                      controller: passwordController,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Password'),
                    ),
                  ),
                ),
              ),
              Container(
                  height: 60,
                  width: 355,
                  padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.grey)),
                    child: const Text('Login'),
                    onPressed: () => loginUser(context),
                  )),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Forget Your Password ",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              Row(
                // ignore: sort_child_properties_last
                children: <Widget>[
                  const Text(
                    "Don't have an account?",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  TextButton(
                    child: const Text(
                      'Register',
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegisterPage(),
                          ));
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          )),
        ));
  }
}

// ignore: must_be_immutable
class RegisterPage extends StatelessWidget {
  RegisterPage({super.key});
  TextEditingController nameFirstController = TextEditingController();
  TextEditingController nameLastController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmController = TextEditingController();

  Future<void> registerUser(context) async {
    String url = 'http://localhost:4000/register';

    Map<String, String> registrationData = {
      'fname': nameFirstController.text,
      'lname': nameLastController.text,
      'email': emailController.text,
      'phone_no': phoneController.text,
      'password': passwordController.text,
      'confirmPassword': confirmController.text,
    };
    try {
      // Send the POST request to the backend API
      http.Response response = await http.post(Uri.parse(url),
          body: jsonEncode(registrationData),
          headers: {"Content-Type": "application/json"});

      if (response.statusCode == 200) {
        // Registration successful, handle the response as needed
        // ignore: avoid_print
        print('User registered successfully');
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: const Text('Register Successfully'),
            actions: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ));
                },
                child: const Text('OK'),
              ),
            ],
          ),
        );
        // You can navigate to the login page or display a success message
      } else {
        // Registration failed, handle the error
        // ignore: avoid_print
        print('Registration failed');
        // You can display an error message to the user
      }
    } catch (error) {
      // Handle any error that occurs during the API request
      // ignore: avoid_print
      print('An error occurred: $error');
      // You can display an error message to the user
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          body: SafeArea(
              child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(18, 50, 0, 0),
            child: Text(
              'Sanoads',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 30, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: nameFirstController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'First Name'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: nameLastController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Last Name'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: phoneController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Phone Number'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: emailController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Email'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password'),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: SizedBox(
              height: 60,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                child: TextField(
                  controller: confirmController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Confirm Password'),
                ),
              ),
            ),
          ),
          Container(
              height: 70,
              width: 360,
              padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.grey)),
                child: const Text('Register'),
                onPressed: () {
                  registerUser(context);
                },
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                "Already have an account?",
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
              TextButton(
                child: const Text(
                  'Login',
                  style: TextStyle(fontSize: 20, color: Colors.grey),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginPage(),
                      ));
                },
              )
            ],
          ),
        ],
      ))),
    );
  }
}
