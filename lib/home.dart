import 'package:flutter/material.dart';

import 'boost.dart';
import 'notification.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        actions: [
          Expanded(
            flex: 1,
            child: Column(children: [
              IconButton(
                icon: const Icon(
                  Icons.rocket_launch,
                  size: 30,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const BoostPage(),
                      ));
                },
              ),
              const Text(
                'Boost Ads',
                style: TextStyle(
                    fontSize: 5,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Container(
              width: 260,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.black,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(25),
              ),
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Row(children: [
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: Text(
                          'Search',
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(160, 0, 0, 0),
                        child: Icon(
                          Icons.search,
                          color: Colors.grey,
                          size: 20,
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Column(children: [
              IconButton(
                icon: const Icon(
                  Icons.notifications,
                  color: Colors.black,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const NotificationPage(),
                      ));
                },
              ),
            ]),
          ),
        ],
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 140,
                  width: 393,
                  color: Colors.white,
                  child: Row(children: [
                    Expanded(
                      flex: 8,
                      child: Container(
                        height: 130,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.black, width: 3)),
                        child: Row(children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              width: 20,
                              color: Colors.white,
                              child: Column(children: [
                                const Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '  App Name',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '  Download This App',
                                      style: TextStyle(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '\n  Short description about app ',
                                      style: TextStyle(
                                          fontSize: 6,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                      children: [
                                        SizedBox(
                                          width: 120,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Apple Store',
                                                style: TextStyle(fontSize: 2),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 100,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Play Store',
                                                style: TextStyle(fontSize: 3),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              1, 20, 0, 0),
                                          child: Image.asset('assets/7.png',
                                              width: 60),
                                        )
                                      ],
                                    )),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                              child: Image.asset(
                                'assets/1.png',
                                scale: 0.2,
                              ),
                            ),
                          )
                        ]),
                      ),
                    ),

                    // side container
                    Expanded(
                      flex: 10,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                        child: Container(
                          height: 130,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border:
                                  Border.all(color: Colors.black, width: 3)),
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Image.asset('assets/4.jpg', width: 50)
                                    ],
                                  )),
                                  const Expanded(
                                      child: Text(
                                    'Dell Laptop For Sale',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 8),
                                  )),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Image.asset('assets/2.png', width: 80)
                                    ],
                                  )),
                                  const Expanded(
                                      child: Column(
                                    children: [
                                      Text(
                                        'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                                        style: TextStyle(
                                            fontSize: 6,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  )),
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Image.asset('assets/10.jpg', width: 70)
                                    ],
                                  )),
                                ],
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Text(
                                        '     Contact: 98XXXXXXXX   Address:Address,Kathmandu',
                                        style: TextStyle(
                                            fontSize: 6,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  )),
                                ],
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
              // Next Container
              Container(
                height: 25,
                width: 393,
                color: Colors.white,
                child: const Column(children: [
                  Expanded(
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Icon(
                            Icons.trending_up,
                            size: 30,
                          ),
                        ),
                        Text(
                          ' Trending Now',
                          style: TextStyle(
                              fontWeight: FontWeight.w400, fontSize: 17),
                        )
                      ],
                    ),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 140,
                  width: 393,
                  color: Colors.white,
                  child: Row(children: [
                    Expanded(
                      flex: 8,
                      child: Container(
                        height: 130,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.black, width: 3)),
                        child: Row(children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              width: 20,
                              color: Colors.white,
                              child: Column(children: [
                                const Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '  App Name',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '  Download This App',
                                      style: TextStyle(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '\n  Short description about app ',
                                      style: TextStyle(
                                          fontSize: 6,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                      children: [
                                        SizedBox(
                                          width: 120,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Apple Store',
                                                style: TextStyle(fontSize: 2),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 100,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Play Store',
                                                style: TextStyle(fontSize: 3),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              1, 20, 0, 0),
                                          child: Image.asset('assets/7.png',
                                              width: 60),
                                        )
                                      ],
                                    )),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                              child: Image.asset(
                                'assets/1.png',
                                scale: 0.2,
                              ),
                            ),
                          )
                        ]),
                      ),
                    ),

                    // side container
                    Expanded(
                      flex: 10,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                        child: Container(
                          height: 140,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border:
                                  Border.all(color: Colors.black, width: 3)),
                          child: Row(children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                width: 20,
                                color: Colors.white,
                                child: Column(children: [
                                  const Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          '  App Name',
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Expanded(
                                          child: Text(
                                        '  Download This App',
                                        style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.w500),
                                      )),
                                    ],
                                  ),
                                  const Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Expanded(
                                          child: Text(
                                        '\n  Short description about app ',
                                        style: TextStyle(
                                            fontSize: 6,
                                            fontWeight: FontWeight.w500),
                                      )),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Column(
                                        children: [
                                          SizedBox(
                                            width: 120,
                                            height: 35,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      1, 10, 0, 0),
                                              child: ElevatedButton.icon(
                                                icon: const Icon(
                                                  Icons.circle,
                                                  color: Colors.grey,
                                                  size: 12,
                                                ),
                                                style: ButtonStyle(
                                                    backgroundColor:
                                                        MaterialStateProperty
                                                            .all(Colors.black)),
                                                label: const Text(
                                                  'Available at\n Apple Store',
                                                  style: TextStyle(fontSize: 3),
                                                ),
                                                onPressed: () {},
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 100,
                                            height: 35,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      1, 10, 0, 0),
                                              child: ElevatedButton.icon(
                                                icon: const Icon(
                                                  Icons.circle,
                                                  color: Colors.grey,
                                                  size: 12,
                                                ),
                                                style: ButtonStyle(
                                                    backgroundColor:
                                                        MaterialStateProperty
                                                            .all(Colors.black)),
                                                label: const Text(
                                                  'Available at\n Play Store',
                                                  style: TextStyle(fontSize: 3),
                                                ),
                                                onPressed: () {},
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                      Expanded(
                                          child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 20, 0, 0),
                                            child: Image.asset('assets/7.png',
                                                width: 60),
                                          )
                                        ],
                                      )),
                                    ],
                                  ),
                                ]),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                                child: Image.asset(
                                  'assets/1.png',
                                  scale: 0.2,
                                ),
                              ),
                            )
                          ]),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
              // 3rd container
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 140,
                  width: 393,
                  color: Colors.white,
                  child: Row(children: [
                    Expanded(
                      flex: 8,
                      child: Container(
                        height: 130,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.black, width: 3)),
                        child: Row(children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              width: 20,
                              color: Colors.white,
                              child: Column(children: [
                                const Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        '  App Name',
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '  Download This App',
                                      style: TextStyle(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                const Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Expanded(
                                        child: Text(
                                      '\n  Short description about app ',
                                      style: TextStyle(
                                          fontSize: 6,
                                          fontWeight: FontWeight.w500),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                      children: [
                                        SizedBox(
                                          width: 120,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Apple Store',
                                                style: TextStyle(fontSize: 2),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 100,
                                          height: 35,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                1, 10, 0, 0),
                                            child: ElevatedButton.icon(
                                              icon: const Icon(
                                                Icons.circle,
                                                color: Colors.grey,
                                                size: 10,
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.black)),
                                              label: const Text(
                                                'Available at\n Play Store',
                                                style: TextStyle(fontSize: 3),
                                              ),
                                              onPressed: () {},
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              1, 20, 0, 0),
                                          child: Image.asset('assets/7.png',
                                              width: 60),
                                        )
                                      ],
                                    )),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                              child: Image.asset(
                                'assets/1.png',
                                scale: 0.2,
                              ),
                            ),
                          )
                        ]),
                      ),
                    ),

                    // side container
                    Expanded(
                      flex: 10,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                        child: Container(
                          height: 140,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border:
                                  Border.all(color: Colors.black, width: 3)),
                          child: Column(children: [
                            Row(
                              children: [
                                Expanded(
                                    child: Column(
                                  children: [
                                    Image.asset('assets/4.jpg', width: 50)
                                  ],
                                )),
                                const Expanded(
                                    flex: 2,
                                    child: Text(
                                      'Dell Laptop For Sale',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    )),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Column(
                                      children: [
                                        Image.asset('assets/2.png', width: 100)
                                      ],
                                    )),
                                const Expanded(
                                    flex: 1,
                                    child: Column(
                                      children: [
                                        Text(
                                          'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                                          style: TextStyle(
                                              fontSize: 6,
                                              fontWeight: FontWeight.w500),
                                        )
                                      ],
                                    )),
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    flex: 2,
                                    child: Column(
                                      children: [
                                        Text(
                                          '     Contact: 98XXXXXXXX \n Address:Address,Kathmandu',
                                          style: TextStyle(
                                              fontSize: 6,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    )),
                                Expanded(
                                    child: Column(
                                  children: [
                                    Image.asset('assets/10.jpg', width: 42)
                                  ],
                                )),
                              ],
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
              // Last container
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 145,
                  width: 393,
                  color: Colors.white,
                  child: Row(children: [
                    Expanded(
                      flex: 13,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 5, 0),
                        child: Container(
                          height: 150,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border:
                                  Border.all(color: Colors.black, width: 3)),
                          child: Column(children: [
                            Row(
                              children: [
                                Expanded(
                                    child: Column(
                                  children: [
                                    Image.asset('assets/4.jpg', width: 50)
                                  ],
                                )),
                                const Expanded(
                                    flex: 2,
                                    child: Text(
                                      'Dell Laptop For Sale',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    )),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                    flex: 1,
                                    child: Column(
                                      children: [
                                        Image.asset('assets/2.png', width: 80)
                                      ],
                                    )),
                                const Expanded(
                                    child: Column(
                                  children: [
                                    Text(
                                      'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                                      style: TextStyle(
                                          fontSize: 6,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                )),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(2, 9, 0, 0),
                              child: Row(
                                children: [
                                  const Expanded(
                                      flex: 2,
                                      child: Column(
                                        children: [
                                          Text(
                                            '   Contact:98XXXXXXX \n Address:Address,Kathamandu',
                                            style: TextStyle(
                                                fontSize: 6,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      )),
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Image.asset('assets/10.jpg', width: 45)
                                    ],
                                  )),
                                ],
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ),
                    // side container
                    Expanded(
                      flex: 13,
                      child: Container(
                        height: 136,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.black, width: 3)),
                        child: Column(children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Column(
                                  children: [
                                    Image.asset('assets/4.jpg', width: 50)
                                  ],
                                )),
                                const Expanded(
                                    child: Text(
                                  'Dell Laptop For Sale',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 8),
                                )),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Column(
                                  children: [
                                    Image.asset('assets/2.png', width: 80)
                                  ],
                                )),
                                const Expanded(
                                    child: Column(
                                  children: [
                                    Text(
                                      'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                                      style: TextStyle(
                                          fontSize: 6,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                )),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              const Expanded(
                                  flex: 2,
                                  child: Column(
                                    children: [
                                      Text(
                                        '     Contact: 98XXXXXXXX \n Address:Address,Kathmandu',
                                        style: TextStyle(
                                            fontSize: 6,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  )),
                              Expanded(
                                  child: Column(
                                children: [
                                  Image.asset('assets/10.jpg', width: 50)
                                ],
                              )),
                            ],
                          ),
                        ]),
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
