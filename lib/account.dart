import 'package:flutter/material.dart';
import 'package:sanoads/edit.dart';

import 'notification.dart';

// ignore: must_be_immutable
class AccountPage extends StatelessWidget {
  final ScrollController controller = ScrollController();

  TextEditingController currentPassowrd = TextEditingController();
  TextEditingController newPassword = TextEditingController();
  TextEditingController confirmNewPassword = TextEditingController();

  AccountPage({super.key});

  get confrimNewPassowrd => null;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[400],
        actions: [
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Row(
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.account_circle_rounded,
                        size: 30,
                        color: Colors.black,
                      ),
                      onPressed: () {},
                    ),
                    const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('Name',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.black)),
                    )
                  ],
                )
              ],
            ),
          ),
          Expanded(
            flex: 0,
            child: Column(
              children: [
                Row(
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.notifications,
                        size: 30,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const NotificationPage(),
                            ));
                      },
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(0),
                child: SizedBox(
                  width: 392,
                  height: 320,
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text(
                                'Basic Information',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 18),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 5, 0, 0),
                        child: Row(
                          children: [
                            Expanded(flex: 2, child: Text('Name')),
                            Expanded(
                                flex: 2,
                                child: Text(
                                  'Name',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
                        child: Row(
                          children: [
                            Expanded(flex: 2, child: Text('Address')),
                            Expanded(
                                flex: 2,
                                child: Text(
                                  'Address',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
                        child: Row(
                          children: [
                            Expanded(flex: 2, child: Text('Contact number')),
                            Expanded(
                                flex: 2,
                                child: Text(
                                  'Contact number',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 15, 0, 0),
                        child: Row(
                          children: [
                            Expanded(flex: 2, child: Text('Email address')),
                            Expanded(
                                flex: 2,
                                child: Text(
                                  'Email address',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(300, 15, 7, 0),
                        child: Column(
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Editpage(),
                                    ));
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Colors.grey)),
                              child: const Text('Edit'),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(0),
                child: Container(
                  width: 392,
                  height: 400,
                  decoration: BoxDecoration(color: Colors.grey[100]),
                  child: Column(
                    children: [
                      const Padding(
                          padding: EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Icon(
                                Icons.settings,
                                color: Colors.black,
                                size: 20,
                              ),
                              Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  'Settings',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          )),
                      const Padding(
                        padding: EdgeInsets.fromLTRB(10, 8, 7, 0),
                        child: Row(
                          children: [
                            Text(
                              'Change password',
                              style: TextStyle(fontWeight: FontWeight.w400),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 60,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                          child: TextField(
                            controller: currentPassowrd,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Current Password'),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 60,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                          child: TextField(
                            controller: newPassword,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'New Password'),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 60,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                          child: TextField(
                            controller: confrimNewPassowrd,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Confirm New Password'),
                          ),
                        ),
                      ),
                      Container(
                          height: 70,
                          width: 400,
                          padding: const EdgeInsets.fromLTRB(7, 10, 7, 10),
                          child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.grey)),
                            child: const Text('Change Password'),
                            onPressed: () {},
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: Row(
                          children: [
                            IconButton(
                              icon: const Icon(
                                Icons.bookmark,
                                color: Colors.black,
                                size: 30,
                              ),
                              onPressed: () {},
                            ),
                            const Text(
                              'Saved',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 16),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  children: [
                    Container(
                      height: 250,
                      width: 339,
                      color: Colors.grey[200],
                      child: const Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Text(
                                  'FAQs',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Text(
                                  'About us',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Text(
                                  'Terms of use',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Text(
                                  'Privacy policy',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Text(
                                  'Costomer Support',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.logout),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: const Text(
                                    'Confirmation',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                  content: const Text(
                                      'Are you sure you want to log out?'),
                                  actions: [
                                    ElevatedButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: const Text('No'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  const LogPage(),
                                            ));
                                      },
                                      child: const Text('Yes'),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                          const Text(
                            'Log Out',
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 18),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
