import 'package:flutter/material.dart';

class BoostPage extends StatelessWidget {
  const BoostPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          "Boost ads",
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              SizedBox(
                height: 679.2,
                width: 400,
                child: Column(children: [
                  const Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(80, 0, 0, 0),
                        child: Icon(
                          Icons.rocket_launch,
                          size: 50,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Boost ads",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 275, 0),
                    child: Text(
                      "Features",
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                    ),
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(30, 10, 0, 0),
                        child: Text(
                          '●  Displays ads on top of search results \n    and the search panel',
                          style: TextStyle(fontSize: 15),
                        ),
                      ))
                    ],
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(30, 10, 0, 0),
                        child: Text(
                          '●  Smart algorithm ensures that listings \n    appear first when people search for \n    related products',
                          style: TextStyle(fontSize: 15),
                        ),
                      ))
                    ],
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(30, 10, 0, 0),
                        child: Text(
                          '●  Powerful targeting options to reach \n    the right audience at the right time',
                          style: TextStyle(fontSize: 15),
                        ),
                      )),
                    ],
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(30, 10, 0, 0),
                        child: Text(
                          '●  Increases visibility and potentail for \n    more clicks and conversions',
                          style: TextStyle(fontSize: 15),
                        ),
                      ))
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Pricing:  Rs.500 per day (24hrs)',
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16),
                          ),
                        )
                      ],
                    ),
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(18, 0, 0, 0),
                        child: Text(
                          'Link to Ad',
                          style: TextStyle(fontSize: 15, color: Colors.grey),
                        ),
                      ))
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                    child: SizedBox(
                      height: 60,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(7, 10, 5, 10),
                        child: TextField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'https//www.sanoads.com/titleofad'),
                        ),
                      ),
                    ),
                  ),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(18, 0, 0, 0),
                        child: Text(
                          'Number of days',
                          style: TextStyle(fontSize: 15, color: Colors.grey),
                        ),
                      ))
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                    child: SizedBox(
                      height: 60,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(7, 10, 7, 10),
                        child: TextField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Select'),
                        ),
                      ),
                    ),
                  ),
                  Container(
                      height: 60,
                      width: 380,
                      padding: const EdgeInsets.fromLTRB(7, 10, 5, 10),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                            Colors.grey,
                          )),
                          child: const Text('BOOST'),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Qrcode(),
                                ));
                          })),
                  const Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: EdgeInsets.fromLTRB(18, 0, 0, 0),
                        child: Text(
                          'Note: The payments made are non-refundable. \nPlease keep the payment receipt.',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ))
                    ],
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class Qrcode extends StatelessWidget {
  const Qrcode({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          "Boost ads",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          const Row(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(80, 0, 0, 0),
                child: Icon(
                  Icons.rocket_launch,
                  size: 60,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  "Boost ads",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 23),
                ),
              ),
            ],
          ),
          const Row(
            children: [
              Expanded(
                  child: Padding(
                padding: EdgeInsets.fromLTRB(50, 25, 0, 0),
                child: Text(
                  'E-Sewa/Fone Pay QR code Scan',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
              ))
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: Image.asset(
              'assets/7.png',
            ),
          ),
        ],
      )),
    );
  }
}
