import 'package:flutter/material.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.grey,
          title: const Text(
            'Categories',
            style: TextStyle(color: Colors.black),
          )),
      body: SafeArea(
        child: Column(children: [
          const Row(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Category",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Image.asset('assets/2.png'),
                    const Text('Electronics')
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Image.asset('assets/2.png'),
                    const Text('Electronics')
                  ],
                ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Image.asset('assets/2.png'),
                    const Text('Electronics')
                  ],
                )),
                Expanded(
                    child: Column(
                  children: [Image.asset('assets/9.png'), const Text('Apps')],
                )),
                Expanded(
                    child: Column(
                  children: [
                    Image.asset('assets/2.png'),
                    const Text('Electronics')
                  ],
                ))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
