import 'package:flutter/material.dart';
import 'package:sanoads/home.dart';

// ignore: use_key_in_widget_constructors
class AddPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          'Create an ad',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(children: [
        Column(children: [
          SizedBox(
            height: 550,
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Title of Ad')],
                  ),
                ),
                const SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Price')],
                  ),
                ),
                const SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: ''),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Contact')],
                  ),
                ),
                const SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: ''),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Location')],
                  ),
                ),
                const SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: ''),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Type of Advertisement')],
                  ),
                ),
                const SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: ''),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [Text('Description')],
                  ),
                ),
                const SizedBox(
                  height: 70,
                  width: 382,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: TextField(
                      maxLines: null,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: ''),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                  width: 382,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.grey),
                      ),
                      child: const Text("Upload Pixture"),
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 200,
              color: Colors.grey,
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  "Picture 3",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 200,
              color: Colors.grey,
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  "Picture 3",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 200,
              color: Colors.grey,
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  "Picture 3",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25, color: Colors.white),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 35,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child: SizedBox(
                      width: 130,
                      child: ElevatedButton(
                        // ignore: sort_child_properties_last
                        child: const Text('Cancel'),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey)),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: const Text(
                                'Confirmation',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                              content:
                                  const Text('Are you sure you want to Quit?'),
                              actions: [
                                ElevatedButton(
                                  onPressed: () => Navigator.pop(context),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.grey)),
                                  child: const Text('No'),
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              const HomePage(),
                                        ));
                                  },
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.grey)),
                                  child: const Text('Yes'),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                    child: SizedBox(
                      width: 130,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const CreatePage()));
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey)),
                        child: const Text('Continue'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ]),
    );
  }
}

class CreatePage extends StatelessWidget {
  const CreatePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          'Create an ad',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(children: [
        Column(
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 210, 0),
              child: Text(
                'Choose a template',
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 250,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 100, 0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [Image.asset('assets/4.jpg', width: 70)],
                        )),
                        const Expanded(
                            child: Column(
                          children: [
                            Text(
                              'Title of ad',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            )
                          ],
                        )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 100, 0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [Image.asset('assets/6.png', width: 110)],
                        )),
                        const Expanded(
                            child: Column(
                          children: [
                            Text(
                              'Price\n Discription \n Features \n Features \n Features',
                            )
                          ],
                        )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Row(
                      children: [
                        const Expanded(
                            flex: 2,
                            child: Column(
                              children: [
                                Text(
                                  '    Contact: 98XXXXXXXX \n Address:Address,Kathamandu',
                                  style: TextStyle(fontSize: 12),
                                ),
                              ],
                            )),
                        Expanded(
                            child: Column(
                          children: [Image.asset('assets/3.jpg', width: 110)],
                        )),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            // next Container
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 250,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 100, 0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [Image.asset('assets/4.jpg', width: 70)],
                        )),
                        const Expanded(
                            child: Column(
                          children: [
                            Text(
                              'Title of ad',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            )
                          ],
                        )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 100, 0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Column(
                          children: [Image.asset('assets/6.png', width: 110)],
                        )),
                        const Expanded(
                            child: Column(
                          children: [
                            Text(
                              'Price\n Discription \n Features \n Features \n Features',
                            )
                          ],
                        )),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      const Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                child: Text(
                                  '  Contact: 98XXXXXXXX \nAddress:Address,Kathmandu',
                                  style: TextStyle(fontSize: 12),
                                ),
                              )
                            ],
                          )),
                      Expanded(
                          child: Column(
                        children: [Image.asset('assets/3.jpg', width: 110)],
                      )),
                    ],
                  ),
                ]),
              ),
            ),
            // 3rd Container
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 250,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Row(children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      width: 50,
                      color: Colors.white,
                      child: Column(children: [
                        const Row(
                          children: [
                            Expanded(
                              child: Text(
                                '  App Name',
                                style: TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                                child: Text(
                              '  Sub heading',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            )),
                          ],
                        ),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                                child: Text(
                              '\n  Short description about app ',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500),
                            )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Column(
                                children: [
                                  SizedBox(
                                    width: 120,
                                    height: 50,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          1, 20, 0, 0),
                                      child: ElevatedButton.icon(
                                        icon: const Icon(
                                          Icons.circle,
                                          color: Colors.grey,
                                        ),
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    Colors.black)),
                                        label: const Text(
                                          'Available at\n Apple Store',
                                          style: TextStyle(fontSize: 8),
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 120,
                                    height: 50,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          1, 20, 0, 0),
                                      child: ElevatedButton.icon(
                                        icon: const Icon(
                                          Icons.circle,
                                          color: Colors.grey,
                                        ),
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    Colors.black)),
                                        label: const Text(
                                          'Available at\n Play Store',
                                          style: TextStyle(fontSize: 8),
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                              Expanded(
                                  child: Column(
                                children: [
                                  Image.asset('assets/7.png', width: 100)
                                ],
                              )),
                            ],
                          ),
                        ),
                      ]),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 200,
                      color: Colors.white,
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Container(
                            color: Colors.grey,
                            height: 160,
                            width: 100,
                            child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'Picture',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                )),
                          ),
                        )
                      ]),
                    ),
                  )
                ]),
              ),
            ),
            // 4th Container
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 250,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Column(children: [
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [Image.asset('assets/4.jpg', width: 70)],
                      )),
                      Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 30, 0),
                                child: Text(
                                  'Dell Laptop For Sale',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Column(
                            children: [Image.asset('assets/2.png', width: 100)],
                          )),
                      const Expanded(
                          child: Column(
                        children: [
                          Text(
                            'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      )),
                    ],
                  ),
                  Row(
                    children: [
                      const Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Text(
                                '     Contact: 98XXXXXXXX \nAddress:Address,Kathmandu',
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          )),
                      Expanded(
                          child: Column(
                        children: [Image.asset('assets/10.jpg', width: 120)],
                      )),
                    ],
                  ),
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 240,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Column(children: [
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [Image.asset('assets/4.jpg', width: 70)],
                      )),
                      const Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Text(
                                'Dell Laptop For Sale',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              )
                            ],
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Column(
                            children: [Image.asset('assets/2.png', width: 100)],
                          )),
                      const Expanded(
                          child: Column(
                        children: [
                          Text(
                            'At only Rs 35,000 \nLightweight \nTB Hard disk \n6 hours battery backup \nLightweight',
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      )),
                    ],
                  ),
                  Row(
                    children: [
                      const Expanded(
                          flex: 2,
                          child: Column(
                            children: [
                              Text(
                                '     Contact: 98XXXXXXXX \nAddress:Address,Kathmandu',
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          )),
                      Expanded(
                          child: Column(
                        children: [Image.asset('assets/10.jpg', width: 120)],
                      )),
                    ],
                  ),
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 250,
                width: 350,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 3)),
                child: Row(children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      width: 50,
                      color: Colors.white,
                      child: Column(children: [
                        const Row(
                          children: [
                            Expanded(
                              child: Text(
                                '  App Name',
                                style: TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                                child: Text(
                              '  Sub heading',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            )),
                          ],
                        ),
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Expanded(
                                child: Text(
                              '\n  Short description about app ',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500),
                            )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Column(
                                children: [
                                  SizedBox(
                                    width: 120,
                                    height: 50,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          1, 20, 0, 0),
                                      child: ElevatedButton.icon(
                                        icon: const Icon(
                                          Icons.circle,
                                          color: Colors.grey,
                                        ),
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    Colors.black)),
                                        label: const Text(
                                          'Available at\n Apple Store',
                                          style: TextStyle(fontSize: 8),
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 120,
                                    height: 50,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          1, 20, 0, 0),
                                      child: ElevatedButton.icon(
                                        icon: const Icon(
                                          Icons.circle,
                                          color: Colors.grey,
                                        ),
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    Colors.black)),
                                        label: const Text(
                                          'Available at\n Play Store',
                                          style: TextStyle(fontSize: 8),
                                        ),
                                        onPressed: () {},
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                              Expanded(
                                  child: Column(
                                children: [
                                  Image.asset('assets/7.png', width: 100)
                                ],
                              )),
                            ],
                          ),
                        ),
                      ]),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
                      child: Image.asset(
                        'assets/1.png',
                        scale: 0.1,
                      ),
                    ),
                  )
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: SizedBox(
                      width: 130,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => (AddPage()),
                              ));
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey)),
                        child: const Text('Back'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(80, 0, 0, 0),
                    child: SizedBox(
                      width: 130,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const PreviewPage()));
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey)),
                        child: const Text('Continue'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ]),
    );
  }
}

class PreviewPage extends StatelessWidget {
  const PreviewPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.grey,
          title: const Text(
            'Create an ad',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
          )),
      body: SafeArea(
          child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 20, 300, 0),
            child: Text(
              'Preview',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 230,
              width: 350,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.black, width: 3)),
              child: Row(children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    width: 50,
                    color: Colors.white,
                    child: Column(children: [
                      const Row(
                        children: [
                          Expanded(
                            child: Text(
                              '  App Name',
                              style: TextStyle(
                                  fontSize: 30, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                              child: Text(
                            '  Sub heading',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500),
                          )),
                        ],
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                              child: Text(
                            '\n  Short description about app ',
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w500),
                          )),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Column(
                            children: [
                              SizedBox(
                                width: 120,
                                height: 50,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(1, 20, 0, 0),
                                  child: ElevatedButton.icon(
                                    icon: const Icon(
                                      Icons.circle,
                                      color: Colors.grey,
                                    ),
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Colors.black)),
                                    label: const Text(
                                      'Available at\n Apple Store',
                                      style: TextStyle(fontSize: 8),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 120,
                                height: 50,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(1, 20, 0, 0),
                                  child: ElevatedButton.icon(
                                    icon: const Icon(
                                      Icons.circle,
                                      color: Colors.grey,
                                    ),
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Colors.black)),
                                    label: const Text(
                                      'Available at\n Play Store',
                                      style: TextStyle(fontSize: 8),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              ),
                            ],
                          )),
                          Expanded(
                              child: Column(
                            children: [Image.asset('assets/7.png', width: 90)],
                          )),
                        ],
                      ),
                    ]),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 29, 0, 0),
                    child: Image.asset(
                      'assets/1.png',
                      scale: 0.1,
                    ),
                  ),
                )
              ]),
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 5, 250, 0),
            child: Text('   Share this ad to:'),
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                  child: Padding(
                padding: EdgeInsets.fromLTRB(20, 5, 0, 0),
                child: Row(
                  children: [
                    Icon(
                      Icons.circle,
                      color: Colors.grey,
                    ),
                    Icon(
                      Icons.circle,
                      color: Colors.grey,
                    ),
                    Icon(
                      Icons.circle,
                      color: Colors.grey,
                    ),
                    Icon(
                      Icons.circle,
                      color: Colors.grey,
                    ),
                  ],
                ),
              )),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: SizedBox(
                    width: 130,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => (const CreatePage()),
                            ));
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey)),
                      child: const Text('Back'),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(80, 0, 0, 0),
                  child: SizedBox(
                    width: 130,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const HomePage()));
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey)),
                      child: const Text('Post'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
