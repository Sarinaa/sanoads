
const {createServer } = require("http")
const {json} = require("express")
const express = require("express")
const app = express();
const cors = require("cors");
const connection = require("./database")
const server = createServer(app);

app.use(json());
app.use(cors({
    origin:'*'
}))

app.post('/register', (req,res)=>{

    const { fname, lname, email, phone_no, password, confirmPassword } = req.body;
    console.log(fname);
    console.log(lname);
    console.log(phone_no);
    console.log(password);
    console.log(email);
    console.log(confirmPassword);

    try {
        if(!fname || !lname || !email || !phone_no || !password || !confirmPassword){
            res.send("Please fill all the fields");
            console.log("Please fill all the fields");
            return;
        }

        else{
            connection.query("SELECT * FROM User WHERE email = ? && phone_number = ?", [email, phone_no], function (err, data) {

                if(err) {
                    console.log(err);
                    res.status(404)
                    res.send("User already exists");
                    return;
                }
                else if (data.length > 0) { 
                    console.log("User alredy exists");
            
                    res.status(404).
                    res.send("User alredy exists");
                    return;
                }
                else if (!fname || !lname || !email || !phone_no || !password) {
                    res.send("Please fill all the fields");
                    console.log("Please fill all the fields");
                    return;
                }
                else if(email.split("@").length ==1){
                    res.send('Please enter valid email')
                    return;
                }
                else if(password !== confirmPassword){
                    console.log("Password doesn't match");
                    res.send("Password doesn't match");
                    return;
                }
                else {
                    connection.query("INSERT INTO user (First_Name, Last_Name, Email, Phone_Number, Password) VALUES(?,?,?,?,?)", [fname, lname, email, phone_no,  password], function (err, data) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("User Successfully Registered");
                            res.send("User successfully registered");
                        }
                    })
                }
            })
            }
        }catch (error) {
            console.log("An error occurred:", error);
        }
    });

    app.post('/login', (req,resp)=>{
        const { email, password } = req.body;
        console.log(email);
        console.log(password);

        try {
            if (!email || !password){
                resp.send('Please Fill all the fields');
                console.log("Please Fill all the fields");
                return;
            }
            else{
                connection.query( "SELECT * FROM user WHERE Email=? && Password =?",
                [email, password],
                function (err, data){
                    if (err) {
                        console.log(err);
                        resp.send("An error occured")
                    }
                    else if (data.length > 0){
                        console.log("Log in Successful");
                        resp.send("Log in Successful");
                    }
                    else{
                        console.log("User doesn't Exist Please register first")
                        resp.send("User doesn't exist");
                    }
                })
            }
        } catch (error) {
            console.log("An error occurred:", error);
        }
    });

app.get('/user', (req, res) => {
    const query = 'SELECT * FROM User';
    connection.query(query, (err, rows) => {
    if (err) {
        console.error('Error executing the database query:', err);
        res.status(500).json({ error: 'Error executing the database query' });
        return;
    }
    res.json(rows);
    });
});


app.listen(4000,() => {
    console.log('Listening to the port 4000')
});

